# Como atualizar sua branch com modificações da master

## atualizar a master local

`git checkout master`

`git fetch -p origin`

`git merge origin/master`

## atualizar a sua branch com as modificações da master

`git checkout sua-branch`

`git merge master`

## enviar suas modificações para o gitlab

`git push`

- entre no gitlab e faça o merge request
